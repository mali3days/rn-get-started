// /* @flow */

// import React from 'react';
// import { ScreenOrientation } from 'expo';

// ScreenOrientation.allow(ScreenOrientation.Orientation.ALL);

// import {
//   Platform,
//   ScrollView,
//   StyleSheet,
//   TouchableOpacity,
//   Text,
//   View,
// } from 'react-native';

// import { SafeAreaView, StackNavigator } from 'react-navigation';

// // import Components
// import HomeScreen from '../screens/HomeScreen';

// const ExampleRoutes = {
//   HomeTab: {
//     name: 'Home',
//     description: '<<Home tab>>',
//     screen: HomeScreen,
//   },
//   // CustomTabs: {
//   //   name: 'Custom Tabs',
//   //   description: 'Custom tabs with tab router',
//   //   screen: CustomTabs,
//   // },
//   // CustomTransitioner: {
//   //   name: 'Custom Transitioner',
//   //   description: 'Custom transitioner with stack router',
//   //   screen: CustomTransitioner,
//   // },
//   // ModalStack: {
//   //   name:
//   //     Platform.OS === 'ios'
//   //       ? 'Modal Stack Example'
//   //       : 'Stack with Dynamic Header',
//   //   description:
//   //     Platform.OS === 'ios'
//   //       ? 'Stack navigation with modals'
//   //       : 'Dynamically showing and hiding the header',
//   //   screen: ModalStack,
//   // },
//   // LinkStack: {
//   //   name: 'Link in Stack',
//   //   description: 'Deep linking into a route in stack',
//   //   screen: SimpleStack,
//   //   path: 'people/Jordan',
//   // },
//   // LinkTabs: {
//   //   name: 'Link to Settings Tab',
//   //   description: 'Deep linking into a route in tab',
//   //   screen: SimpleTabs,
//   //   path: 'settings',
//   // },
// };

// const MainScreen = ({ navigation }) => (
//   <ScrollView style={{ flex: 1 }} contentInsetAdjustmentBehavior="automatic">
//     <Banner />
//     {Object.keys(ExampleRoutes).map((routeName: string) => (
//       <TouchableOpacity
//         key={routeName}
//         onPress={() => {
//           const { path, params, screen } = ExampleRoutes[routeName];
//           const { router } = screen;
//           const action = path && router.getActionForPathAndParams(path, params);
//           navigation.navigate(routeName, {}, action);
//         }}
//       >
//         <SafeAreaView
//           style={styles.itemContainer}
//           forceInset={{ vertical: 'never' }}
//         >
//           <View style={styles.item}>
//             <Text style={styles.title}>{ExampleRoutes[routeName].name}</Text>
//             <Text style={styles.description}>
//               {ExampleRoutes[routeName].description}
//             </Text>
//           </View>
//         </SafeAreaView>
//       </TouchableOpacity>
//     ))}
//   </ScrollView>
// );

// export default { ExampleRoutes, MainScreen };
